# docker-novnc-baseimage



## Minimal noVNC + Openbox + LXDE + RDP

An Ubuntu 20.04 base Linux image with [tigervnc](https://tigervnc.org/) VNC server + [openbox](http://openbox.org/) Window manager + [LXDE](https://www.lxde.org/) desktop apps.

This is a base to which you can add XWindow apps such as Matlab, cisTEM, Mathematica, KNIME, etc.

For those who want to use RDP, xrdp is also included.

## How to build

The build-test script first copies the 'self.pem' file into the config subdirectory then invokes a docker buil of the container. The self.pem file contains an https site certificate for your server and is used by [websockify](https://github.com/novnc/websockify) -- a lightweight web/wss server included in the container -- to provide WSS and https access to the noVNC javascript and index.html page. The format of self.pem is site_certificate_key forllowed by site_certificate forllowed by the rest of the certificate key chain. An alternative to this would be to handle the ssl and WSS certs and crypto in an external proxy like NGINX.

## How to run

The run-example script will kill any previous instances of the cotnainer, then start a new instance the listens at port 6080. The VNCPASS environmental variable is used to set the password for the container instance. So that users have a persistent home directory, we volume mount a directoruy from the host server to /home/ubuntu in the container. We need to have some settings files in place for the ubuntu user - copy the contents of the user-template directory to wherever you are stashing the user's persistent home directory as a starting point.

Note that this minimal example turns off nearly everything from the LXDE desktop and openbox so that there are not many menu items in the user interface. If you want to add/change this update the files 

          user-template/.config/menus/lxde-applications.menu
          user-template/.config/openbox/menu.xml

If you want to add icons to the Desktop you can do so in the directory

          user-template/Desktop

This is where the Firefox icon is defined.


## How to access

If you construct a URL in this format - the user will be automatically logged in since you are passing the password in the URL

https://test-az-00.oit.duke.edu:6080/index.html?encrypt=1&autoconnect=1&password=badpassw&port=6080&resize=remote

Note however that the security of this is a bit weak - the vncpasswd command wants passwords that are 6-8 characters long. You can use a longer password but only the first 8 characters are significant. Workarounds for this include putting an NGINX proxy in front ot the service and enforcing ProxyAuth at the NGINX service -or- perhaps fixing the tigernVNC vncpasswd code.

Another note: if you pass in the paramater resize=remote in thye URL, when the web browser window is resized, the remote X desktop is also resized. You probably want this...

You can also access the container via an RDP client (like Microsoft's Remote Desktop app). You will need to log in as the useur 'ubuntu' with the password assigned to that container user in the UBUNTUPASS parameter at container startup. 

## Startup and Customization

At runtime, the docker container calls 'tini' to run the script start-vnc.sh. In this script we set the VNC password and store it outside the ubuntu user's home directopry (since we want to keep this file out of the user's persistent home directory).

We then launch the VNC server (and Xwindows) with the tigervncserver command, wait 2 seconds and kill the vncconfig process with is automaticallyt launched at intitial startup and is not something users really need to see. Finally we start the noVNC which launched the websockify mini web server/WSS service.

The shell script needs to stick around so that the docker container doesn't termimnate, so the script ends wirth a 'wait' statement -- this means that the bash shell will hang around until all the child processes termiante (i.e. until tigervnc and noVNC/websockify die).

If you need additional processes to be launched at container startup, startup-vnc.sh is the place to do that.